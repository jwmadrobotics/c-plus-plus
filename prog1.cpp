#include "prog1.h"

int ReadStudents(std::istream &input, StudentInfo Info[]) {
  int index = 0;
  std::string currentId;
  while(input >> currentId) { 
    Info[index].id = currentId;
    int total;
    for(int i = 0; i < NUMGRADES; i++) {
      int temp;
      input >> temp;
      Info[index].grades[i] = temp;
      total += temp; 
    }

    Info[index].avGrade = total / (double) NUMGRADES;
  }
  return index;

}

void HighestAverage(StudentInfo Info[], int numStudents,std::string &id, double &maxAv) {
  maxAv = Info[0].avGrade;
  id = Info[0].id;
  for(int i = 1; i < numStudents; i++) {
    if (Info[i].avGrade > maxAv) {
      maxAv = Info[i].avGrade; 
      id = Info[i].id;
    }
  }
}
