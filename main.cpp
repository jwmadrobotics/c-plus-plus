#include <fstream>
#include <iostream>
#include "prog1.h"

int main(int numargs, char *args[]) {
  std::ifstream inFile; 
  
  if (numargs < 2) {
    std::cerr << "usage: " << args[0] << " <file name> " << std::endl; 
    exit(1); 
  }
  
  inFile.open(args[1]); 
  
  if (inFile.fail()) {
    std::cerr << "Unable to open" << args[1] << std::endl; 
    exit(1); 
  }
  StudentInfo info[100]; 
  int numStudents = ReadStudents(inFile, info);
  std::string highestId;
  double highestAvg;
  HighestAverage(info, numStudents, highestId, highestAvg);

  std::cout << "Student " << highestId << " has the highest average: " << highestAvg << std::endl;

}
