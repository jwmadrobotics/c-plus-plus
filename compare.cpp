#include <iostream>
#include <fstream>

void Compare(std::istream& inputA, std::istream & inputB, std::ostream &output) {
  char c;
  char d; 
  bool equal = true;
  while(inputA >> c && inputB >> d) {
    if ( c != d) {
      equal = false; 
    }
  }
  if(equal) {
    output << "equal" << std::endl;
  } else {
    output << "not equal" << std::endl; 
  }

}

int main() {
  std::ifstream inFile1; 
  std::ifstream inFile2; 
  inFile1.open("input1.dat");
  inFile2.open("input2.dat"); 
  if(inFile1.fail() || inFile2.fail()) {
    std::cerr << "unable to open the file" << std:: endl; 
    return 1;
  }
  Compare(inFile1, inFile2, std::cout); 
  return 0; 
}
