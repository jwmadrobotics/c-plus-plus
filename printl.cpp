#include <iostream>
struct ListNode {
  int data;
  ListNode *next;  // pointer to the next node in the list
};
void PrintList(ListNode * ptr) {
  ListNode * current = ptr; 
  while(ptr != NULL) {
    std::cout << ptr->data << std::endl; 
  }
}
