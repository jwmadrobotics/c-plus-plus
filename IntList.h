#include <iostream>

class IntList {
  public: 
    IntList(); 
    IntList(int n, int v); 
    void AddToEnd(int k);
    void Print(std::ostream &output) const; 
    int Length() const;
  private:
    static const int SIZE = 10;
    int *Items;
    int numItems;
    int arraySize;

};


