#include <iostream>

int main() {
  int sum; 
  int x;
  for(int i = 1; i <= 10; i++) {
    std::cout << "Enter the number for " << i << " : ";
    std::cin >> x; 
    sum += x;
    std::cout << std::endl; 
  }
  std::cout << "The sum is " << sum << std::endl; 
  return 0;
}
