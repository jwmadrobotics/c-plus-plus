#include <string>
#include <fstream>

const int NUMGRADES = 5;

struct StudentInfo {
  std::string id;
  int grades[NUMGRADES];
  double avGrade;
};

int ReadStudents(std::istream &input, StudentInfo Info[] );
void HighestAverage(StudentInfo Info[], int numStudents, std::string &id, double &maxAv); 

