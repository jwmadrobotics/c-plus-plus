#include "SortedList.h"
#include <fstream>

int main(int argc, char* argv[]) {
  std::ifstream inFile1;
  std::ifstream inFile2;
//  cout << "begininga1";
  if (argc < 3) {
    std::cerr << "not enough arguments" << std::endl;
    exit(1);
  }

  inFile1.open(argv[1]);
  if (inFile1.fail()) {
    std::cerr << "unable to open first file: " << argv[1] << std::endl;
    exit(1);
  }


  inFile2.open(argv[2]);
  if (inFile2.fail()) {
    std::cerr << "unable to open second file: " << argv[2] << std::endl;
    exit(1);
  }
 // cout << "after open file 2";

  SortedList sl; 
  std::string str;
  while(inFile1 >> str) {
  //  cout << str;

    sl.Insert(str);
  }
   //cout << "got here";
  sl.Print(std::cout); //debug
   //cout << "got here";

  while(inFile2 >> str) {
    std::cout << str << ": Occurred " << sl.Lookup(str) << " times." << std::endl; 
  }
  
}
