#include "SortedList.h"
SortedList::SortedList() {
  head = nullptr;
}

void SortedList::Insert(std::string s) {
  ListNode *previous = head;
  ListNode *current = nullptr;

  ListNode* temp = new ListNode();
  temp->data = s;

  if (previous == nullptr) {
    temp->next = nullptr;
    head = temp;
    return; 
  }
  
  if(head->data > s) { //bug fix
    temp->next = head;
    head = temp; 
    return;
  }
  current = previous->next; 

  /* goes through list until it reaches null or the string is before s */
  while (current != nullptr && current->data < s) {
      previous = current;
      current = current->next;
  } 

  temp->next = current;
  previous->next = temp;
  return; 
}

int SortedList::Lookup(std::string s) const {
  ListNode* current = head;
  int count = 0; 
  while(current != nullptr) {
    if(current->data == s) {
      count++;
    }
    current = current->next;
  }
  //std::cout << "For string \"" << s << "\", the count was: " << count << std::endl;
  
  return count;
}

void SortedList::Print(std::ostream &output) const {
  output << "[ "; 

  ListNode* current = head;

  while(current != nullptr) {
    output << current->data << " ";
    current = current->next;
  }
  output << "]" << std::endl;
} /*
void SortedList::Print() const {
  cout << "[ "; 

  ListNode* current = head;

  while(current != nullptr) {
    cout << current->data << " ";
    current = current->next;
  }
  cout << "]" << std::endl;
}
*/
