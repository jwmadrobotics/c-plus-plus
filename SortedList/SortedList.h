#include <iostream>
#include <string>

class SortedList {
  public:
    SortedList();
    
    void Insert(std::string s);
    
    int Lookup(std::string s) const;
    void Print(std::ostream &output) const;

    
  private:
    struct ListNode {
      std::string data;
      ListNode *next;
    };
    
    ListNode *head;
};
