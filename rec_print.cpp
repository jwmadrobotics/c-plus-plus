#include <iostream>
struct ListNode {
  int data;
  ListNode *next;  // pointer to the next node in the list
};
void PrintReverse(ListNode * ptr) {

  if (ptr->next == NULL) return; 
  PrintReverse(ptr->next); 
  std::cout << ptr -> data << std::endl;
}
