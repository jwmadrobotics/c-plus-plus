#include <iostream>

bool arr_eq(int a[], int b[], int length);

int main() {
  int arrA[5] = {1,2,3,4,5}; 
  int arrB[5] = {1,2,3,4,5}; 
  int arrC[5] = {1,3,3,2,5}; 

  std::cout << arr_eq(arrA, arrB, 5) << std::endl;
  std::cout << arr_eq(arrB, arrC, 5) << std::endl;

}
bool arr_eq(int a[], int b[], int length) {
  for(int i = 0; i < length; i++) {
    if(a[i] != b[i]) {
      return false; 
    }
  }
  return true;
}
